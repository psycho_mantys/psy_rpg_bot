#!/usr/bin/env bash

if [ -z "${TELEGRAM_BOT_TOKEN}" ] ; then
	export TELEGRAM_BOT_TOKEN="$1"
fi
if [ -z "${TELEGRAM_UID}" ] ; then
	export TELEGRAM_UID="$2"
fi

curl -w "@tests/curl-format.txt" -o /dev/null -s "https://api.telegram.org/bot${TELEGRAM_BOT_TOKEN}/sendMessage?chat_id=${TELEGRAM_UID}&text=Test"

