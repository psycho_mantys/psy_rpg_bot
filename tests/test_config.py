import pytest

import os
from config import Config

input_for_vars=[
	'111',
	'',
	'_',
	'""""""""""""""',
	':::::::::::::::::::::================'
]
config_vars=[
	"TELEGRAM_BOT_TOKEN",
	"TELEGRAM_BOT_USER_ID_FILE",
]

# fixtures
@pytest.fixture(
	scope="module",
	params=config_vars
)
def unset_env(request):
	print("unset {} if exist".format(request.param))
	if request.param in os.environ:
		del os.environ[request.param]
	return request.param


# Tests
def test_environment_var_is_unset(unset_env):
	print('Create config with "{}" unset'.format(unset_env))
	config=Config()
	assert(None==getattr(config, unset_env))

@pytest.mark.parametrize("config_var", config_vars)
def test_environment_var_is_set(config_var):
	for value in input_for_vars:
		print('Set "{}" with "{}"'.format(config_var, value))
		os.environ[config_var]=value

		print('Create config with "{}" set to "{}"'.format(config_var, value))
		config=Config()
		assert(value==getattr(config, config_var))

def test_interface():
	config=Config()

	for attr_name in config_vars:
		print('Test if create config with "{}" set to "{}"'.format(attr_name, os.environ[attr_name]))
		assert( hasattr(config, attr_name) )
#		assert(all([hasattr(config, attr_name) for attr_name in config_vars]))

