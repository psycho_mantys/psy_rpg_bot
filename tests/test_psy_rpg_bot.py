import pytest

import os
from config import Config
from psy_rpg_bot import Psy_rpg_bot

psy_rpg_bot_inteface=[
	"tmpl_secret_roll",
	"tmpl_secret_roll_tagged",
	"tmpl_roll_tagged",
	"roll",
	"roll_max",
	"roll_min",
	"roll_secret",
	"register",
	"help",
	"start",
	"dm_roll",
	"mention_roll",
	"reply_roll",
	"error",
	"genesys"
]

# MOCKs
class mock_dispatcher(object):
	def __init__(self, bot):
		self.bot=bot

	def add_handler(self,*args,**kwargs):
		pass
	def add_error_handler(self,*args,**kwargs):
		pass


class mock_bot(object):
	def __init__(self):
		self.username="psy_rpg_bot"
		self.last_msg=None

	def send_message(self,**kwargs):
		self.last_msg=kwargs

class mock_message(object):
	def __init__(self, from_user):
		self.text=None
		self._reply_text=None
		self.from_user=from_user

	def reply_text(self, msg):
		#print("!!!"+str(msg))
		self._reply_text=str(msg)

class mock_update(object):
	def __init__(self):
		self.message=None

	def send_message(self,*args,**kwargs):
		pass

class mock_from_user(object):
	def __init__(self, id, username):
		self.id=id
		self.username=username


# fixtures
@pytest.fixture(
	scope="function"
)
def init_bot(request=None):
	config=Config()
	config.TELEGRAM_BOT_USER_ID_FILE='user_id.pickle'
	bot=mock_bot()
	dispatcher=mock_dispatcher(bot)
	psy_rpg_bot=Psy_rpg_bot(config,bot,dispatcher)
	return psy_rpg_bot

@pytest.fixture(
	scope="function"
)
def init_update():
	update=mock_update()
	update.message=mock_message(None)
	update.message.from_user=mock_from_user(101122, "psy_rpg_bot")

	return update


# Tests
@pytest.mark.parametrize(
	"test_msg, expected",[
		("/roll_max d4", "[4]"),
		("/roll_max d8", "[8]"),
		("/roll_max", "No dice_expr"),
	]
)
def test_command_roll_max(init_bot, init_update, test_msg, expected):
	init_update.message.text=test_msg

	args=test_msg.split(' ',1)[1:]

	#print("!!! {!r}".format(str(args)))

	init_bot.roll_max(
		init_bot.bot,
		init_update,
		args
	)

	assert(init_update.message._reply_text==expected)

@pytest.mark.parametrize(
	"test_msg, expected",[
		("/roll_min d4", "[1]"),
		("/roll_min d8", "[1]"),
		("/roll_min", "No dice_expr"),
	]
)
def test_command_roll_min(init_bot, init_update, test_msg, expected):
	init_update.message.text=test_msg

	args=test_msg.split(' ',1)[1:]

	#print("!!! {!r}".format(str(args)))

	init_bot.roll_min(
		init_bot.bot,
		init_update,
		args
	)

	assert(init_update.message._reply_text==expected)


@pytest.mark.parametrize(
	"test_msg, expected",[
		("/roll 1d1", "[1]"),
		("/roll 1d1 OPA", "OPA : [1]"),
		("/roll 2d1", "[1, 1]"),
		("/roll 2d1 #LOCO", "#LOCO : [1, 1]"),
		("/roll 2d1 #LOCO que noia", "#LOCO que noia : [1, 1]"),
		("/roll 2d1 #LOCO   que noia", "#LOCO   que noia : [1, 1]"),
		("/roll 10d1t", "10"),
		("/roll (8,8,8)", "[8, 8, 8]"),
		("/roll", "No dice_expr"),
	]
)
def test_command_roll(init_bot, init_update, test_msg, expected):
	init_update.message.text=test_msg

	args=test_msg.split(' ',1)[1:]

	init_bot.roll(
		init_bot.bot,
		init_update,
		args
	)
	assert(init_update.message._reply_text==expected)

@pytest.mark.parametrize(
	"test_msg, expected",[
		("/genesys r", "Invalid dice: r"),
		("/genesys aaar", "Invalid dice: r"),
		("/genesys raaaa", "Invalid dice: r"),
		("/genesys rrrrrrr", "Invalid dice: r"),
		("/genesys 2a", "Invalid dice: 2"),
		("/genesys 2a    ", "    : Invalid dice: 2"),
		("/genesys ", "No dice_expr"),
		("/genesys        ", "No dice_expr"),
		("/genesys", "No dice_expr"),
	]
)
def test_command_genesys_errors(init_bot, init_update, test_msg, expected):
	init_update.message.text=test_msg

	args=test_msg.split(' ',1)[1:]

	init_bot.genesys(
		init_bot.bot,
		init_update,
		args
	)
	assert(init_update.message._reply_text==expected)



@pytest.mark.parametrize(
	"test_msg, expected",[
		("1d1", "[1]"),
		("1d1 OPA", "OPA : [1]"),
		("2d1", "[1, 1]"),
		("2d1 #LOCO", "#LOCO : [1, 1]"),
		("2d1 #LOCO que noia", "#LOCO que noia : [1, 1]"),
		("2d1 #LOCO   que noia", "#LOCO   que noia : [1, 1]"),
		("10d1t", "10"),
		("(8,8,8)", "[8, 8, 8]"),
	]
)
def test_direct_message_roll(init_bot, init_update, test_msg, expected):
	init_update.message.text=test_msg

	init_bot.dm_roll(
		init_bot.bot,
		init_update
	)

	assert(init_update.message._reply_text==expected)

@pytest.mark.parametrize(
	"test_msg, expected",[
		("@psy_rpg_bot 1d1", "[1]"),
		("@psy_rpg_bot 1d1 OPA", "OPA : [1]"),
		("@psy_rpg_bot 2d1", "[1, 1]"),
		("@psy_rpg_bot 2d1 #LOCO", "#LOCO : [1, 1]"),
		("@psy_rpg_bot 2d1 #LOCO que noia", "#LOCO que noia : [1, 1]"),
		("@psy_rpg_bot 2d1 #LOCO   que noia", "#LOCO   que noia : [1, 1]"),
		("@psy_rpg_bot 10d1t", "10"),
		("aaaa @psy_rpg_bot (8,8,8)", None),
		("d4 @psy_rpg_bot", None),
		("too long for me end @psy_rpg_bot (8,8,8)", None),
		("______@psy_rpg_bot", None),
		("@psy_rpg_bot", "No dice_expr"),
	]
)
def test_mention_roll(init_bot, init_update, test_msg, expected):
	init_update.message.text=test_msg

	init_bot.mention_roll(
		init_bot.bot,
		init_update
	)

	assert(init_update.message._reply_text==expected)


@pytest.mark.parametrize(
	"test_msg, username, user_id",[
		("/register", "psycho_mantys", 42),
		("/register", "lula", 1313),
		("/register", "zero", 0),
		("/register", "zerozero", 00),
		("/register aaaaaaaaaaaa", "zerozero", 00),
		("/register aaaa      a a a      aaaaa", "zerozero", 00),
	]
)
def test_register(init_bot, init_update, test_msg, username, user_id):
	init_update.message.from_user=mock_from_user(user_id, username)

	init_bot.register(
		init_bot.bot,
		init_update
	)

	assert(init_update.message._reply_text==None)
	assert(init_bot.user!=None)
	assert(len(init_bot.user.print())>=1)
	assert(init_bot.user.get_userid("@"+username)==user_id)



@pytest.mark.parametrize(
	"test_msg, expected, from_username",[
		("/roll_secret 1d1 @psycho_mantys", "@mario whisper a roll(1d1)\n[1]", "mario"),
		("/roll_secret 1d1 @psycho_mantys OPA", "@mario whisper a roll(1d1) tagged \"OPA\"\n[1]", "mario"),
		("/roll_secret 2d1 @psycho_mantys", "@mario whisper a roll(2d1)\n[1, 1]", "mario"),
		("/roll_secret 2d1 @psycho_mantys #LOCO", "@mario whisper a roll(2d1) tagged \"#LOCO\"\n[1, 1]", "mario"),
		("/roll_secret 2d1 @psycho_mantys #LOCO que noia", "@mario whisper a roll(2d1) tagged \"#LOCO que noia\"\n[1, 1]", "mario"),
		("/roll_secret 2d1 @psycho_mantys #LOCO   que    noia", "@mario whisper a roll(2d1) tagged \"#LOCO   que    noia\"\n[1, 1]", "mario"),
		("/roll_secret 10d1t 42", "@mario whisper a roll(10d1t)\n10", "mario"),
		("/roll_secret 10d1t 0", "@mario whisper a roll(10d1t)\n10", "mario"),
		pytest.param("/roll_secret 10d10", "", "mario", marks=pytest.mark.xfail),
		pytest.param("/roll_secret", "", "mario", marks=pytest.mark.xfail),
	]
)
def test_roll_secret(init_bot, init_update, test_msg, expected, from_username):
	init_update.message.from_user=mock_from_user(42, from_username)
	init_update.message.text=test_msg

	args=test_msg.split(' ',1)[1:]
	print(args)

	init_bot.roll_secret(
		init_bot.bot,
		init_update,
		args
	)

	assert(init_bot.bot.last_msg.pop("text")==expected)



def test_interface(init_bot):
	for attr_name in psy_rpg_bot_inteface:
		print('Test if Psy_rpg_bot have {!r}'.format(attr_name))
		assert( hasattr(init_bot, attr_name) )
#		assert(all([hasattr(config, attr_name) for attr_name in config_vars]))

