#!/usr/bin/env python

import inspect
import os
from telegram.ext import CommandHandler


class export_commands(object):
	def __init__(self, parent):
		register_methods_as_commands(self, parent.dispatcher)
		self.parent=parent

def register_methods_as_commands(obj, dispatcher):
	for name, func in inspect.getmembers(obj, predicate=inspect.ismethod):
		if name[0]!='_':
			num_param=len(inspect.signature(func).parameters)
			#print("/"+name+" => "+str(func)+" , "+str(num_param))
			if num_param<3:
				dispatcher.add_handler(CommandHandler(name, func))
			else:
				dispatcher.add_handler(CommandHandler(name, func, pass_args=True))

def run_bot(bot):
	if os.environ.get('BOT_WEBHOOK_ENABLE', False):
		# start the bot
		bot.updater.start_webhook(
			listen="0.0.0.0",
			port=int(os.environ.get('BOT_WEBHOOK_PORT', '8080')),
			url_path=bot.config.TELEGRAM_BOT_TOKEN,
			webhook_url=os.environ.get('BOT_WEBHOOK_BASE_URL')+bot.config.TELEGRAM_BOT_TOKEN
		)
		bot.updater.bot.set_webhook(os.environ.get('BOT_WEBHOOK_BASE_URL')+bot.config.TELEGRAM_BOT_TOKEN)

		# run the bot until you press ctrl-c or the process receives sigint,
		# sigterm or sigabrt. this should be used most of the time, since
		# start_polling() is non-blocking and will stop the bot gracefully.
		bot.updater.idle()
	else:
		# Start the Bot
		bot.updater.start_polling()

		# Run the bot until you press Ctrl-C or the process receives SIGINT,
		# SIGTERM or SIGABRT. This should be used most of the time, since
		# start_polling() is non-blocking and will stop the bot gracefully.
		bot.updater.idle()

def list_get(l, idx, default=None):
	try:
		return l[idx]
	except IndexError:
		return default

