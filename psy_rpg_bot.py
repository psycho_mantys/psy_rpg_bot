#!/usr/bin/env python

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters, Dispatcher
from telegram.messageentity import MessageEntity
from telegram.error import NetworkError
import dice
import redis

from queue import Queue

import os

import pickle

import commands
import helper
import genesys
#from sql_helper import Sql_helper

from helper import list_get

class Psy_rpg_bot(object):

	tmpl_secret_roll="@{} whisper a roll({})\n{}"
	tmpl_secret_roll_tagged='@{} whisper a roll({}) tagged "{}"\n{}'

	tmpl_roll_tagged='{tag} : {rolled}'

	tmpl_no_dice_expr='No dice_expr'

	tmpl_genesys_roll='Success="{SUCCESS}" Advantages="{ADVANTAGE}" Triumph="{TRIUMPH}" Black="{BLACK}" White="{WHITE}"'

	def __init__(self, config, bot=None, dispatcher=None):
		self.config=config
		self.user=None

		#if os.path.isfile(config.TELEGRAM_BOT_USER_ID_FILE):
		#	with open(config.TELEGRAM_BOT_USER_ID_FILE, 'rb') as f:
		#		self.user=pickle.load(f)
		#else:
		#	self.user=dict()
		#	self.user["@psycho_mantys"]=1376807781
		#	self.user["@baltazar_tavares"]=110308372
		self.user=redis.StrictRedis(
			host=config.REDIS_HOST,
			port=config.REDIS_PORT,
			db=config.REDIS_DB,
			password=config.REDIS_PASSWORD
		)

		if bot:
			self.bot=bot
			self.update_queue=Queue()
			if not dispatcher:
				self.dispatcher=Dispatcher(bot, self.update_queue)
			else:
				self.dispatcher=dispatcher
			self.TELEGRAM_BOT_USERNAME=bot.username
		else:
			# Create the EventHandler and pass it your bot's token.
			#print("token set: {}".format(TELEGRAM_BOT_TOKEN))
			self.updater=Updater(self.config.TELEGRAM_BOT_TOKEN)
			self.bot=self.updater.bot
			# Get the dispatcher to register handlers
			self.dispatcher=self.updater.dispatcher
			self.TELEGRAM_BOT_USERNAME=self.updater.bot.username

		#self.user=Sql_helper("table_"+self.TELEGRAM_BOT_USERNAME, os.environ['DATABASE_URL'])


		# log all errors
		self.dispatcher.add_error_handler(self.error)

		"""Bind commands(/somethings)"""
		self.commands=commands.Commands(self)

		# any mention on start of message
		self.dispatcher.add_handler(MessageHandler(
			Filters.text & Filters.entity(MessageEntity.MENTION), self.mention_roll)
		)
		# message is not a group chat and text to bot
		self.dispatcher.add_handler(MessageHandler(Filters.text & ~Filters.group, self.dm_roll))

	#def __del__(self):
	#	if self.user:
	#		with open(self.config.TELEGRAM_BOT_USER_ID_FILE, 'wb') as f:
	#			pickle.dump(self.user, f, pickle.HIGHEST_PROTOCOL)

	def reply_roll(self, update, dice_expr, roller, tag=None):
		self.config.logger.warning('roll for "%s"', dice_expr)
		if not dice_expr:
			update.message.reply_text(self.tmpl_no_dice_expr)
		elif tag:
			update.message.reply_text(self.tmpl_roll_tagged.format(
				tag=tag, rolled=roller(dice_expr)
			))
		else:
			update.message.reply_text(roller(dice_expr))


	def dm_roll(self, bot, update):
		msg=update.message.text.split(' ', 1)
		dice_expr=list_get(msg, 0)
		tag=list_get(msg, 1)
		roller=dice.roll

		self.reply_roll(update, dice_expr, roller, tag)

	def mention_roll(self, bot, update):
		msg=update.message.text.split(' ', 2)
		if msg[0]=="@"+self.TELEGRAM_BOT_USERNAME:
			dice_expr=list_get(msg, 1)
			tag=list_get(msg, 2)
			roller=dice.roll

			self.reply_roll(update, dice_expr, roller, tag)

	def error(self, bot, update, error):
		"""Log Errors caused by Updates."""
		self.config.logger.warning('Update "%s" caused error "%s"', update, error)


	def start(self, bot, update):
		"""Send a message when the command /start is issued."""
		update.message.reply_text("""
Welcome!

Bot created by @psycho_mantys, if you have any suggestions, talk to me.

For more help or to know the commands, use the command "/help"
""")
		self.register(bot, update)

	def help(self, bot, update):
		"""Send a message when the command /help is issued."""
		update.message.reply_markdown("""
/roll /roll@psy\_rpg\_bot dice\_expression \[tag]
/r /r@psy\_rpg\_bot dice\_expression \[tag]

Roll a dice expression and show the results.

For more examples about dice expression, [see only docs](https://github.com/psychomantys/psy_rpg_bot/wiki/Examples-for-dice-expression).

/roll\_max /roll\_min dice\_expression \[tag]
/r\_max /r\_min dice\_expression \[tag]

Like /r, but with lowest or highest results.

/register

Map telegram username to user ID. This is required for /roll\_secret.

/roll\_secret /roll\_secret@psy\_rpg\_bot dice\_expression USER\_ID \[tag]
/rs /rs@psy\_rpg\_bot dice\_expression USER\_ID \[tag]

Roll a dice\_expression, tag, and send to USER\_ID. USER\_ID is telegram user\_id, but can be a user name(@something) if the user executed /register on bot chat.

/genesys /genesys@psy\_rpg\_bot dice\_expression

Roll dice of genesys system. Suport force dice. To dice expr, [see only docs](https://github.com/psychomantys/psy_rpg_bot/wiki/Examples-for-dice-expression-in-genesys-and-FFG-Star-Wars).

""")

	def roll_secret(self, bot, update, args):
		msg=update.message.text.split(' ', 3)
		dice_expr=list_get(msg, 1)
		user_id=list_get(msg, 2)
		tag=list_get(msg, 3)

		if not user_id or not dice_expr:
			update.message.reply_text("usage: /rs dice_expr user [TAG]")
			return None


		from_user=update.message.from_user.username

		if user_id[0]=='@':
			#user_id=self.user[user_id]
			#user_id=self.user.get_userid(user_id)
			user_id=int(self.user.get(user_id))

		self.config.logger.warning('roll secret from "%s" to "%s" for "%s"', from_user, user_id, dice_expr)

		if tag:
			bot.send_message(chat_id=user_id, text=self.tmpl_secret_roll_tagged.format(
				from_user,
				dice_expr,
				tag,
				str(dice.roll(dice_expr))
			))
		else:
			bot.send_message(chat_id=user_id, text=self.tmpl_secret_roll.format(
				from_user,
				dice_expr,
				str(dice.roll(dice_expr))
			))

	def register(self, bot, update, args=None):
		nid=update.message.from_user.id
		username=update.message.from_user.username

		self.config.logger.warning('Register user "@%s" with uid "%s"', username, nid)
		#self.user.del_username("@"+username)
		#self.user["@"+username]=nid
		self.user.set("@"+username, nid)
		#if self.user.is_valid(nid):
		#	self.user.set_user_field(nid, "username", "@"+username)
		#else:
		#	self.user.set_user(nid, "@"+username)


	def roll(self, bot, update, args=None):
		msg=update.message.text.split(' ', 2)
		dice_expr=list_get(msg, 1)
		tag=list_get(msg, 2)
		roller=dice.roll

		self.reply_roll(update, dice_expr, roller, tag)

	def roll_max(self, bot, update, args):
		msg=update.message.text.split(' ', 2)
		dice_expr=list_get(msg, 1)
		tag=list_get(msg, 2)
		roller=dice.roll_max

		self.reply_roll(update, dice_expr, roller, tag)

	def roll_min(self, bot, update, args):
		msg=update.message.text.split(' ', 2)
		dice_expr=list_get(msg, 1)
		tag=list_get(msg, 2)
		roller=dice.roll_min

		self.reply_roll(update, dice_expr, roller, tag)

	def genesys(self, bot, update, args):
		msg=update.message.text.split(' ', 2)
		dice_expr=list_get(msg, 1)
		tag=list_get(msg, 2)
		roller=genesys.roll

		self.reply_roll(update, dice_expr, roller, tag)

