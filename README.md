RPG Helper Bot para o Telegram [![Build Status](https://travis-ci.org/psychomantys/psy_rpg_bot.png)](https://travis-ci.org/psychomantys/psy_rpg_bot)
=================================================

BOT com algumas coisas uteis para jogar RPG em grupos. Pode ser adicionado em grupos.

Onde Conseguir o token?
=================================================

Você pode criar e conseguir o token atraves do BotFather do telegram.

Comece uma conversa com ele e veja como conseguir o token e criar seu bot.

Existe alguns documentos online para isso também.


Baixar o código
=================================================

Para baixar o código fonte, utilizamos o git e o endereço do repositório atual. O endereço do repositório é:

```bash
https://github.com/psychomantys/psy_rpg_bot.git
```

Você pode usar qualquer cliente gráfico do git, mas se você tiver em um ambiente de linha de comando, resumidamente você pode fazer:

```bash
git clone https://github.com/psychomantys/psy_rpg_bot.git
```

Criar ambiente de desenvolvimento
=================================================

Para configurar o ambiente de de desenvolvimento, vamos usar o ```pipenv```.

Para mais detalhes, consulte a documentação do `pipenv`.

Instalando as dependências de desenvolvimento
--------------------------------------------------------

Para gerenciar as dependências de desenvolvimento, iremos usar o `pipenv`.

O `pipenv` só deve ser utilizado uma vez no inicio depois de baixar o código do repositório. Para isso, em um ambiente de linha de comando e considerando que, você deve digitar:

```shell
cd psy_rpg_bot
pipenv install
```

Esse código vai criar um ambiente para o código fonte com as ferramenta do python.

Caso o python do seu sistema não seja por padrão o usado no projeto(no caso o python3), você deve especificar a versão do python no ```pipenv```, executando no lugar das linhas acima o seguinte:

```bash
cd psy_rpg_bot
pipenv install --two
```

Preferivelmente, use a versão mais nova do python.

Entrando no ambiente virtual do python
--------------------------------------------------------

Para ativar esse ambiente e usar as ferramentas desse ambiente, execute:

```bash
pipenv shell
```

Saindo do ambiente virtual do python
--------------------------------------------------------

Quando terminar de executar e instalar dependências nesse ambiente, simplesmente saia do shell.

Usar o Bot
=================================================

Para usar o bot, vocẽ deve executar ele e manter ele rodando em qualquer maquina.

Para passar o token do telegram, você pode inserir ele diretamente no arquivo `config.py` ou passar o token pela linha de comando como por exemplo:

```bash
pipenv run ./__init__.py --token-telegram '5222333:ASDSDsdsadsa_Sdasd344VCV'
```

Ou por exemplo:

```bash
pipenv run ./__init__.py -t '7222333:ASDSDsdsadsa_Sdasd344VCV'
```

Ou por exemplo:

```bash
export TELEGRAM_BOT_TOKEN='7222333:ASDSDsdsadsa_Sdasd344VCV'
pipenv run ./__init__.py
```

Este token não deve ser valido e é apenas um exemplo.

Executar os testes
=================================================

O repositorio tem integração continua com o travis.

Para executar os testes, você pode fazer:

```bash
pipenv run python setup.py test
```

Preferencialmente, execute os testes antes de subir o codigo para o repositorio.

