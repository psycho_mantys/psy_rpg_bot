#!/usr/bin/env python
# -*- coding: utf-8 -*-

#from telegram.ext import MessageHandler, Filters
from subprocess import Popen, PIPE
from optparse import OptionParser

from config import Config
from psy_rpg_bot import Psy_rpg_bot

import helper

def main():
	config=Config()
	

	parser=OptionParser()
	parser.add_option(
		"-t", "--token-telegram",
		help=("Token recived from FatherBot")
	)
	parser.add_option(
		"-f", "--userid-file",
		help=("File to persist userid")
	)

	options, args = parser.parse_args()
	
	if options.token_telegram is not None:
		config.TELEGRAM_BOT_TOKEN=options.token_telegram
	if options.userid_file is not None:
		config.TELEGRAM_BOT_USER_ID_FILE=options.userid_file

	"""Start the bot."""
	rpgbot=Psy_rpg_bot(config)

	helper.run_bot(rpgbot)

def create_from_bot(bot):
	config=Config()

	config.logger.warning('INIT')

	config.TELEGRAM_BOT_USER_ID_FILE="userid_file.pickle"

	"""Start the bot."""
	rpgbot=Psy_rpg_bot(config, bot)

	return rpgbot

if __name__ == '__main__':
	main()

