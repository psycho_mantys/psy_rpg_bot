from os import environ
import logging

class Config(object):
	def __init__(self):
		self.TELEGRAM_BOT_TOKEN=environ.get('TELEGRAM_BOT_TOKEN')
		self.TELEGRAM_BOT_USER_ID_FILE=environ.get('TELEGRAM_BOT_USER_ID_FILE')

		self.REDIS_HOST=environ.get('REDIS_HOST')
		self.REDIS_PORT=environ.get('REDIS_PORT')
		self.REDIS_DB=environ.get('REDIS_DB')
		self.REDIS_PASSWORD=environ.get('REDIS_PASSWORD')

		# Enable logging
		logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
			level=logging.INFO)

		self.logger=logging.getLogger(__name__)

