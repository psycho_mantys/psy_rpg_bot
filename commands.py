
import helper

class Commands(helper.export_commands):
	def start(self, bot, update):
		self.parent.start(bot, update)

	def help(self, bot, update):
		self.parent.help(bot, update)


	def roll_secret(self, bot, update, args=None):
		self.parent.roll_secret(bot, update, args)
	def rs(self, bot, update, args=None):
		self.parent.roll_secret(bot, update, args)

	def register(self, bot, update):
		self.parent.register(bot, update)

	def r(self, bot, update, args):
		self.parent.roll(bot, update, args)
	def roll(self, bot, update, args=None):
		self.parent.roll(bot, update, args)

	def r_max(self, bot, update, args):
		self.parent.roll_max(bot, update, args)
	def roll_max(self, bot, update, args):
		self.parent.roll_max(bot, update, args)

	def r_min(self, bot, update, args):
		self.parent.roll_min(bot, update, args)
	def roll_min(self, bot, update, args):
		self.parent.roll_min(bot, update, args)

	def genesys(self, bot, update, args):
		self.parent.genesys(bot, update, args)

