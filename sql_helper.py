import os
import psycopg2
import psycopg2.extras


class Sql_helper(object):
	def __init__(self, table_name, database_url):
		self.DATABASE_URL=database_url

		self.conn=psycopg2.connect(self.DATABASE_URL)

		self.table_name=table_name
		#print("self.table_name: "+self.table_name)
		
	def create_tables(self):

		#cur=self.conn.cursor()
		#cur.execute("DROP TABLE {TABLE_NAME}".format(TABLE_NAME=self.table_name))
		#cur.close()
		#self.conn.commit()

		create_table="""
		CREATE TABLE {TABLE_NAME} (
			id INTEGER PRIMARY KEY,
			username TEXT
		);""".format(TABLE_NAME=self.table_name)
		cur=self.conn.cursor()
		try:
			cur.execute(create_table)
		except Exception as e:
			cur.close()
			raise e

		self.conn.commit()

	def del_userid(self, user_id):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""
			DELETE FROM {TABLE_NAME} WHERE id = %s
		""".format(
			TABLE_NAME=self.table_name
		)
		cur.execute(sql, (user_id,))
		cur.close()
		self.conn.commit()

	def del_username(self, username):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""
			DELETE FROM {TABLE_NAME} WHERE username = %s
		""".format(
			TABLE_NAME=self.table_name
		)
		cur.execute(sql, (username,))
		cur.close()
		self.conn.commit()


	def get_user(self, user_id):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""
			SELECT * from {TABLE_NAME} WHERE id = {USER_ID}
		""".format(
			TABLE_NAME=self.table_name,
			USER_ID=user_id
		)
		cur.execute(sql)
		user=cur.fetchall()
		if len(user):
			return user[0]
		else:
			return None

	def is_valid(self, user_id):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""
			SELECT * from {TABLE_NAME} WHERE id = %s
		""".format(
			TABLE_NAME=self.table_name
		)
		cur.execute(sql, (user_id,))
		user=cur.fetchall()
		if len(user):
			return all(user)
		else:
			return False

	def get_user_field(self, user_id, field):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""SELECT * from {TABLE_NAME} WHERE id = %s""".format(
			TABLE_NAME=self.table_name
		)
		cur.execute(sql, (user_id,) )
		user=cur.fetchall()[0]
		return user[field]

	def get_userid(self, username):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""
			SELECT * from {TABLE_NAME} WHERE username = %s
		""".format(
			TABLE_NAME=self.table_name
		)
		cur.execute(sql, (username,) )
		user=cur.fetchall()[0]
		return user["id"]



	def set_user(self, user_id, username):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""
		INSERT INTO {TABLE_NAME} (
			id,
			username
		) VALUES (%s,%s)
		""".format(TABLE_NAME=self.table_name)
		null=None
		try:
			cur.execute(sql, (user_id, username))
		except Exception as e:
			cur.close()
			raise e
		self.conn.commit()

	def set_user_field(self, user_id, field, data):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""
			UPDATE {TABLE_NAME} SET {FIELD} = %s WHERE id = %s
		""".format(
			TABLE_NAME=self.table_name,
			FIELD=field
		)
		cur.execute(sql, (data, user_id))
		cur.close()
		self.conn.commit()


	def print(self):
		cur=self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		sql="""
			SELECT * from {TABLE_NAME}
		""".format(
			TABLE_NAME=self.table_name
		)
		cur.execute(sql)
		user=cur.fetchall()
		return user
		if len(user):
			return user[0]
		else:
			return None




if __name__ == '__main__':
	x=Sql_helper("table_psy_rpg_bot", os.environ['DATABASE_URL'])

	print(x.print())
	#x.create_tables()
	#x.set_user("11")
	#print(x.get_user("11"))
	#print(x.get_user_field("11","id"))
	#print(x.get_user_field("11","fullname"))
	#print("username: "+x.get_userid("@psycho_mantys"))
	#x.set_user_field("110308372","username", "@changed")
	#print("username: "+x.get_userid("@changed"))
	#print("phone: "+x.get_user_field("11","phone"))
	#print(x.is_valid("11"))

