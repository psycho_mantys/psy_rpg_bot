#!/usr/bin/env python

import random

DISPAIR='D'
FAILURE='f'
THREAT='t'

TRIUMPH='T'
ADVANTAGE='a'
SUCCESS='s'

# Star Wars Force die...
BLACK='b'
WHITE='w'

boost=[
	[],
	[],
	[SUCCESS],
	[SUCCESS, ADVANTAGE],
	[ADVANTAGE, ADVANTAGE],
	[ADVANTAGE]
]
setback=[
	[],
	[],
	[FAILURE],
	[FAILURE],
	[THREAT],
	[THREAT]
]
ability=[
	[],
	[SUCCESS],
	[SUCCESS],
	[SUCCESS, SUCCESS],
	[ADVANTAGE],
	[ADVANTAGE],
	[SUCCESS, ADVANTAGE],
	[ADVANTAGE, ADVANTAGE]
]
difficulty=[
	[],
	[FAILURE],
	[FAILURE, FAILURE],
	[THREAT],
	[THREAT],
	[THREAT],
	[THREAT, THREAT],
	[FAILURE, THREAT]
]
proficiency=[
	[],
	[SUCCESS],
	[SUCCESS],
	[SUCCESS, SUCCESS],
	[SUCCESS, SUCCESS],
	[ADVANTAGE],
	[SUCCESS, ADVANTAGE],
	[SUCCESS, ADVANTAGE],
	[SUCCESS, ADVANTAGE],
	[ADVANTAGE, ADVANTAGE],
	[ADVANTAGE, ADVANTAGE],
	[TRIUMPH, SUCCESS]
]
challenge=[
	[],
	[FAILURE],
	[FAILURE],
	[FAILURE, FAILURE],
	[FAILURE, FAILURE],
	[THREAT],
	[THREAT],
	[FAILURE, THREAT],
	[FAILURE, THREAT],
	[THREAT, THREAT],
	[THREAT, THREAT],
	[DISPAIR, FAILURE]
]
force=[
	[BLACK],
	[BLACK],
	[BLACK],
	[BLACK],
	[BLACK],
	[BLACK],
	[BLACK, BLACK],
	[WHITE],
	[WHITE],
	[WHITE, WHITE],
	[WHITE, WHITE],
	[WHITE, WHITE]
]


def roll(dice_expr):
	result={}

	result['success']=0
	result['advantage']=0

	result['triumph']=0
	result['dispair']=0

	result['black']=0
	result['white']=0

	#print("dice_expr: "+dice_expr)
	for gdice in dice_expr:
		#print("gdice: "+gdice)
		if gdice=="p":
			# Yellow/proficiency
			ret=random.choice(proficiency)
			result['success']+=ret.count(SUCCESS)
			result['advantage']+=ret.count(ADVANTAGE)
			result['triumph']+=ret.count(TRIUMPH)
		elif gdice=="a":
			# Green/ability
			ret=random.choice(ability)
			result['success']+=ret.count(SUCCESS)
			result['advantage']+=ret.count(ADVANTAGE)
			result['triumph']+=ret.count(TRIUMPH)
		elif gdice=="b":
			# blue/boost
			ret=random.choice(boost)
			result['success']+=ret.count(SUCCESS)
			result['advantage']+=ret.count(ADVANTAGE)
			result['triumph']+=ret.count(TRIUMPH)
		elif gdice=="c":
			# red/challenge
			ret=random.choice(challenge)
			result['success']-=ret.count(FAILURE)
			result['advantage']-=ret.count(THREAT)
			result['dispair']+=ret.count(DISPAIR)
		elif gdice=="d":
			# difficulty/purple
			ret=random.choice(difficulty)
			result['success']-=ret.count(FAILURE)
			result['advantage']-=ret.count(THREAT)
			result['dispair']+=ret.count(DISPAIR)
		elif gdice=="s":
			# setback/black
			ret=random.choice(setback)
			result['success']-=ret.count(FAILURE)
			result['advantage']-=ret.count(THREAT)
			result['dispair']+=ret.count(DISPAIR)
		elif gdice=="F":
			# Star Wars force die
			ret=random.choice(force)
			#print(ret)
			result['white']+=ret.count(WHITE)
			result['black']+=ret.count(BLACK)
		else:
			return "Invalid dice: "+gdice

	ret={}
	for k, v in result.items():
		#print( (k,v) )
		if k=='success' or v!=0:
			ret[k]=v

	return ret

